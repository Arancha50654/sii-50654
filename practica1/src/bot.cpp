/*Practica3-Añado el bot que se encarga de controlar la raqueta1 de forma automática, mediante el mecanismo de comunicaciń de ficheros proyectados en memoria (compartidos)*/
//Librerías necesarias para el bot
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h> 
#include <unistd.h>
//Incluye la definicion de la clase que contiene los datos compartidos
#include "DatosMemCompartida.h"

int main() {
	//Declaracion de una variable de tipo puntero a DatosMemCompartida
	DatosMemCompartida* pMemC;
	//Abrimos el fichero proyectado en memoria creado anteriormente en el tenis y proyectado en memoria
	int file;
	char* org;
	file=open("/tmp/datosCompartidos.txt",O_RDWR);
	org=(char*)mmap(NULL,sizeof(*(pMemC)),PROT_WRITE|PROT_READ,MAP_SHARED,file,0);
	//Cerrar el descriptor de fichero
	close(file);
	//Asignar la dirección de comienzo de la región creada al atributo de tipo puntero
	pMemC=(DatosMemCompartida*)org;
	if (pMemC->contador>=10.0f)
		pMemC->contador=10.0f;
	//Bucle infinito
	while(1){
		/*En cada iteracion hay que añadir la lógica necesaria para el bot (en función de las coordenadas Y)
		de la esfera y la raqueta establezca la accion a realizar*/
		//posicion del punto medio de la raqueta
		float posiciony=(pMemC->raqueta1.y2+pMemC->raqueta1.y1)/2;
		if (posiciony<pMemC->esfera.centro.y)
			pMemC->accion=1;
		else if (posiciony>pMemC->esfera.centro.y)
			pMemC->accion=-1;
		else
			pMemC->accion=0;
		//posicion del punto medio de la raqueta2
		float posicion2y=(pMemC->raqueta2.y1+pMemC->raqueta2.y2)/2;
		//controlar la raqueta2
		if (posicion2y<pMemC->esfera.centro.y) 
			pMemC->accion2=1;
		else if (posicion2y>pMemC->esfera.centro.y)
			pMemC->accion2=-1;
		else
			pMemC->accion2=0;
		//Suspender durente 25ms utilizando la llamada a
		usleep(25000);
	}
	/*borra las ubicaciones para el rango de direcciones especificado, y produce referencias a las direcciones
	dentro del rango a fin de generar referencias a memoria inválidas.*/
	munmap(org,sizeof(*(pMemC)));
}

