// MundoCliente.cpp: implementation of the CMundoCliente class.
//
//////////////////////////////////////////////////////////////////////

#include <iostream>//Practica3-libreria necesaria para las cadenas

#include <fstream>
#include "MundoCliente.h"//Practica4-Se cambia Mundo por MundoCliente
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <error.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoCliente::CMundoCliente()
{
	Init();
}

CMundoCliente::~CMundoCliente()
{
	//Practica5-cerrar socket
	comunicacion.Send("Finalizar comunicacion",strlen ("Finalizar comunicacion"));
	comunicacion.Close();
	/*Practica5-eliminar las tuberías entre cliente y servidor	
	//Practica4-Cerrar la tubería (fd2,envia las coordenadas del servidor al cliente) adecuadamente
	close(fd2);
	unlink("/tmp/mififo2");
	//cierro la tubería (fd3,envia las teclas pulsadas del cliente al servidor) adecuadamente
	close(fd3);
	unlink("/tmp/mififo3");
	//Practica3-Cerramos la tubería, enviamos una cadena al logger para indicar que el programa termina
	/*Practica4-MundoCliente no interacciona con el logger (tuberia FIFO)*/
	/*Practica3-borra las ubicaciones para el rango de direcciones especificado, y produce referencias a las direcciones
	dentro del rango a fin de generar referencias a memoria inválidas.*/
	munmap(org,sizeof(MemC));
}

void CMundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	//Practica2-Dibujar disparos
	for (i=0; i<disparos.size();i++) {
		disparos[i].Dibuja();
	}

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoCliente::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}
	
	//Practica2-Movimiento disparos
	for (i=0; i<disparos.size();i++) {
		disparos[i].Mueve(0.025f);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		//los puntos empiezan a contar cuando se ejecuta el bot
		//CONTADOR		
		//if (pMemC->contador<=10.0f) {
		puntos2++;
		//Practica3-enviamos los datos cuando el jugador2 gana un punto
		/*Practica4-MundoCliente no interacciona con el loger (tuberia FIFO)*/
		//}
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		//los puntos empiezan a contar cuando se ejecuta el bot
		//CONTADOR
		//if (pMemC->contador<=10.0f) {
		puntos1++;
		//Practica3-enviamos los datos cuando el jugador1 gana un punto
		/*Practica4-MundoCliente no interacciona con el loger (tuberia FIFO)*/
		//}
	}

	//Practica2-Disminuir tamaño esfera
	esfera.DisminuirTamanio();
	//Practica2-Choque entre disparos y raquetas
	for (int i=0; i<disparos.size(); i++) {
		if (jugador1.Choque(disparos[i])){
			jugador1.Disminuir(0.5,0.5,1.5);
			//Eliminar el disparo que ha chocado
			for (int j=i; j<disparos.size(); j++)
				disparos[j]=disparos[j+1];	
		}
		else if (jugador2.Choque(disparos[i])){
			jugador2.Disminuir(0.5,0.5,1.5);
			//Eliminar el disparo que ha chocado
			for (int j=i; j<disparos.size(); j++)
				disparos[j]=disparos[j+1];
		}
	}
	jugador1.ContarTiempo();
	jugador2.ContarTiempo();
	/*Practica3-actualizamos los campos del atributo DatosMemCompartida 
	e invocar al metodo OnKeyboardDown según el valor de la acción correspondiente*/
	pMemC->esfera=esfera;
	pMemC->raqueta1=jugador1;
	switch(pMemC->accion) {
		case 1: OnKeyboardDown('w',0,0);
			break;
		case -1: OnKeyboardDown('s',0,0);
			break;
		case 0: break;
	}
	/*actualizamos los campos del atributo DatosMemCompartida y se gestiona el movimiento del jugador2 mediante el bot*/
	//CONTADOR
	pMemC->raqueta2=jugador2;
	if (pMemC->contador>=0)
		pMemC->contador=pMemC->contador-0.025;
	if (pMemC->contador<0) {
		switch(pMemC->accion2) {
			case 1: jugador2.velocidad.y=4;
				OnKeyboardDown('o',0,0);
				break;
			case -1:jugador2.velocidad.y=-4;
				OnKeyboardDown('l',0,0);
				break;
			case 0: break;
		}
	}
	//El juego finaliza cuando uno de los jugadores alcance los 3 puntos
	if(puntos1==3 || puntos2==3)
		exit(0);
	//Practica4-Leemos los datos de la tubería (servidor-cliente)
	char cad2[200];
	//Practica5-eliminar las tuberías entre cliente y servidor
	//read(fd2,cad2,sizeof(cad2));
	//recibe las coordenadas a través del socket
	comunicacion.Receive(cad2,200);
	//actualizamos los atributos de MundoCliente con los datos recibidos
	sscanf(cad2,"%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2);
	//Practica3
	pMemC->esfera=esfera;
	pMemC->raqueta1=jugador1;
	pMemC->raqueta2=jugador2;
}

void CMundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	//Practica2-Jugador1 dispara
	case 'd': {Esfera* d=new Esfera(jugador1.x1+0.1,((jugador1.y1+jugador1.y2)/2),3,0);
		 disparos.push_back(*d);break;}
	case 'l':jugador2.velocidad.y=-4;
		 //CONTADOR
		 if (pMemC->contador>0)
		 	pMemC->contador=10.0f;
		 break;
	case 'o':jugador2.velocidad.y=4;
		 //CONTADOR
		 if (pMemC->contador>0)
		 	pMemC->contador=10.0f;
		 break;
	//Practica2-Jugador2 dispara
	case 'k':{Esfera *d2=new Esfera(jugador2.x1-0.1,((jugador2.y1+jugador2.y2)/2),-3,0);
		 disparos.push_back(*d2);break;}
	}
	//Practica4-Escribimos los datos de la tubería (cliente-servidor)
	//Practica5-eliminar las tuberías entre cliente y servidor
	char cad3[TAM_BUFF];
	sprintf(cad3,"%c",key);
	//write(fd3,cad3,sizeof(cad3));
	//envía la tecla a través del socket
	comunicacion.Send(cad3,TAM_BUFF);
}

void CMundoCliente::Init()
{
	Plano p;
	//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

	//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	//Practica3-Abrir la tubería en modo escritura
	/*Practica4-MundoCliente no interacciona con el loger (tuberia FIFO)*/
	//Practica3-Creamos un fichero del tamaño del atributo DatosMemCompartida
	int file=open("/tmp/datosCompartidos.txt",O_RDWR|O_CREAT|O_TRUNC,0666);
	if (file==-1)
		perror("Error de creación o apertura datosCompartidos\n");
	else {
		printf("Creada y abierta datosCompartidos\n"); }
	write(file,&MemC,sizeof(MemC));
	//Proyectar el fichero en memoria, uilizando el atributo de la clase
	org=(char*)mmap(NULL,sizeof(MemC),PROT_WRITE|PROT_READ,MAP_SHARED,file,0);
	//Cerrar el descriptor de fichero
	close(file);
	//Asignar la dirección del comienzo de la región creada al atributo de tipo puntero org
	pMemC=(DatosMemCompartida*)org;
	//Guardamos la posicion inicial de la raqueta1 en la memoria compartida
	MemC.raqueta1=jugador1;
	pMemC->accion=0;
	//Guardamos la posicion inicial de la raqueta y el valor del contador
	//CONTADOR
	pMemC->accion2=0;
	MemC.raqueta2=jugador2;
	pMemC->contador=25.0f;
	//Practica5-eliminar las tuberías entre cliente y servidor
	/*
	//Practica4-Creamos la tubería (servidor-cliente) con nombre (fd2 envia las coordenadas del servidor al cliente)
	int tb2=mkfifo("/tmp/mififo2",0777);
	//compruebo si se crea la tubería (servidor-cliente) de forma correcta
	if (tb2==-1)
		perror("Error de creación mififo2\n");
	else {
		printf("Creada mififo2\n"); }
	//creamos la tubería (cliente-servidor) con nombre (fd3 envia las teclas pulsadas del cliente al servidor)
	int tb3=mkfifo("/tmp/mififo3",0777);
	//compruebo si se crea la tubería (cliente-servidor) de forma correcta
	if (tb3==-1)
		perror("Error de creación mififo3\n");
	else {
		printf("Creada mififo3\n"); }
	//abrimos la tubería (servidor-cliente) en modo lectura
	fd2=open("/tmp/mififo2",O_RDONLY);
	if(fd2==-1)
		perror("Error de apertura mififo2\n");
	else {
		printf("Abierta la tubería mififo2\n"); }
	//abrimos la tubería (cliente-servidor) en modo escritura
	fd3=open("/tmp/mififo3",O_WRONLY);
	if(fd3==-1)
		perror("Error de apertura mififo3\n");
	else {
		printf("Abierta la tubería mififo3\n"); }*/
	//Practica5-socket
	//se conecta a la IP y puerto del servidor
	
	char nombre[TAM_BUFF];
	//pide por teclado un nombre para enviárselo al servidor
	printf("introduzca su nombre\n");
	scanf("%s",nombre);
	comunicacion.Connect((char *)"127.0.0.1",3000);//Comprobar dirección
	//Envía el nombre a través del socket
	comunicacion.Send(nombre,TAM_BUFF);
}
