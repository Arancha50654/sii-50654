// Raqueta.cpp: implementation of the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#include <math.h>
#include "Raqueta.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Raqueta::Raqueta()
{
	tiempo=0;
	seg=0;
}

Raqueta::~Raqueta()
{

}

void Raqueta::Mueve(float t)
{
	//Practica2-Ecuacion para mover la Raqueta
	y1=y1+velocidad.y*t;
	y2=y2+velocidad.y*t;
}

//Practica2-Disminuir el tamaño durante un tiempo
void Raqueta::ContarTiempo() {
	tiempo+=0.01;
	if (seg!=0 && tiempo>=seg /*&& (fabs(y1)+fabs(y2))<2*/) {
		float m=(y1+y2)/2;
		y1=m-1;
		y2=m+1;
		seg=0;
	}
}

void Raqueta::Disminuir(float d1, float d2, float t){
	seg=tiempo+double(t);
	float m=(y1+y2)/2;
	if ((y2-y1)>(d1+d2) || (y1-y2)>(d1+d2)) {
		y1=m-1+d1;
		y2=m+1-d2;
	}
}
