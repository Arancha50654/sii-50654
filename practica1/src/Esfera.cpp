// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.6f;
	velocidad.x=3;
	velocidad.y=3;
	r=g=255;
	b=0;
}

//Practica2-Disparos, sobrecarga del constructor (crear disparos)
Esfera::Esfera(float posicionx, float posiciony, float velocidadx, float velocidady){
	radio=0.1f;
	velocidad.x=velocidadx;
	velocidad.y=velocidady;
	r=255;
	g=b=0;
	centro.x=posicionx;
	centro.y=posiciony;
}

Esfera::~Esfera()
{
	
}



void Esfera::Dibuja()
{
	glColor3ub(r,g,b);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	//Practica2-Ecuacion para mover la pelota
	centro=centro+velocidad*t;
}

//Practica2-Disminuir tamaño esfera
void Esfera::DisminuirTamanio() {
	//La pelota disminuye el tamaño segun avanza el tiempo: cada 5segundos hasta un mínimo
	if (radio>=0.2) 
		radio = radio - 0.002;
	else if (radio<=0.2)
		radio = 0.6;
}
