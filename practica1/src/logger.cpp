//Practica3-Añado el logger que recibe, a traves de una tubería con nombre, del juego una cadea de texto (se debe ejecutar antes que el tenis)
//Librerías necesarias para el logger
#include <stdio.h>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h> 
#include <unistd.h>
#include <string.h>

#define TAM_BUFF 60 //Constante con la que definimos el tamaño maximo de la cadena(buffer)

using namespace std;

int main(){
	//creamos la tubería
	mkfifo("/tmp/mififo",0777);
	//abro la tubería en modo lectura
	int fd=open("/tmp/mififo",O_RDONLY);
	//el logger entra en un bucle infinito
	while(1){
		char buff[TAM_BUFF];
		//recibo los datos de tenis por la tubería
		read(fd,buff,sizeof(buff));
		//imprime el mensaje por salida estándar
		printf("%s",buff);
		//codigo que se utiliza para salir del bucle cuando se sale de tenis de forma inesperada
		char cad[]= "END\n";
		if(strcmp(cad,buff)==0)
			break;
			
	}
	//destruimos la tubería
	close(fd);
	unlink("/tmp/mififo");
	return 0;
}
