// MundoServidor.cpp: implementation of the CMundoServidor class.
//
//////////////////////////////////////////////////////////////////////

#include <iostream>//Practica3-libreria necesaria para las cadenas

#include <fstream>
#include "MundoServidor.h"//Practica4-Se cambia Mundo por MundoServidor
#include "signal.h"/*Practica4-señales*/
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <error.h>

#define PUERTO 3550

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void* hilo_comandos(void* d);//Practica4

void* hilo_comandos2(void* d);

void* hilo_sockets(void* d);//Practica5

void matar_proceso(int signum);//señales

CMundoServidor::CMundoServidor()
{
	Init();
}

CMundoServidor::~CMundoServidor()
{
	//Practica3-Cerramos la tubería, enviamos una cadena al logger para indicar que el programa termina
	sprintf(buff,"END\n");
	write(fd,buff,sizeof(buff));
	close(fd);
	/*borra las ubicaciones para el rango de direcciones especificado, y produce referencias a las direcciones
	dentro del rango a fin de generar referencias a memoria inválidas.*/
	/*Practica4-MundoServidor no interacciona con el bot*/
	//Practica5-eliminar las tuberías entre cliente y servidor
	//Practica4-Cerrar la tubería (servidor-cliente) adecuadamente
	//close(fd2);
	//cerrar la tubería (servidor-cliente) adecuadamente
	//close(fd3);
	//Practica5-cerrar socket
	comunicacion.Close();
	conexion.Close();
}

void CMundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	//Practica2-Dibujar disparos
	for (i=0; i<disparos.size();i++) {
		disparos[i].Dibuja();
	}

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoServidor::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}
	
	//Practica2-Movimiento disparos
	for (i=0; i<disparos.size();i++) {
		disparos[i].Mueve(0.025f);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		//Practica3-los puntos empiezan a contar cuando se ejecuta el bot
		/*Practica4-MundoServidor no interacciona con el bot(memoria compartida)*/
		puntos2++;
		//Practica3-enviamos los datos cuando el jugador2 gana un punto
		sprintf(buff,"El jugador 2 marca 1 punto, lleva un total de %d puntos\n",puntos2);
		write(fd,buff,sizeof(buff));
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		//los puntos empiezan a contar cuando se ejecuta el bot
		/*Practica4-MundoServidor no interacciona con el bot(memoria compartida)*/
		puntos1++;
		//Practica3-enviamos los datos cuando el jugador1 gana un punto
		sprintf(buff,"El jugador 1 marca 1 punto, lleva un total de %d puntos\n",puntos1);
		write(fd,buff,sizeof(buff));
	}

	//Practica2-Disminuir tamaño esfera
	esfera.DisminuirTamanio();
	//Practica2-Choque entre disparos y raquetas
	for (int i=0; i<disparos.size(); i++) {
		if (jugador1.Choque(disparos[i])){
			jugador1.Disminuir(0.5,0.5,1.5);
			//Eliminar el disparo que ha chocado
			for (int j=i; j<disparos.size(); j++)
				disparos[j]=disparos[j+1];	
		}
		else if (jugador2.Choque(disparos[i])){
			jugador2.Disminuir(0.5,0.5,1.5);
			//Eliminar el disparo que ha chocado
			for (int j=i; j<disparos.size(); j++)
				disparos[j]=disparos[j+1];
		}
	}
	jugador1.ContarTiempo();
	jugador2.ContarTiempo();
	/*Practica3-actualizamos los campos del atributo DatosMemCompartida 
	e invocar al metodo OnKeyboardDown según el valor de la acción correspondiente*/
	/*Practica4-MundoServidor no interacciona con el bot(memoriacompartida)*/
	//El juego finaliza cuando uno de los jugadores alcance los 3 puntos
	if(puntos1==3 || puntos2==3)
		exit(0);
	//Practica4-Construir la cadena de texto con los datos a enviar
	//Practica5-eliminar las tuberías entre cliente y servidor
	//Practica5-envía las coordenadas a través del socket
	char cad2[200];
	sprintf(cad2,"%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x,esfera.centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2, jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);
	
	//Practica4-enviar los datos por la tubería (servidor-cliente)
	//write(fd2,cad2,sizeof(cad2));
	//Practica5-recibe el nombre del cliente conectado a través del socket y lo imprime por pantalla
	char micadena[TAM_BUFF];
	comunicacion=conexion.Accept();
	comunicacion.Receive(micadena,TAM_BUFF);
	printf("Nombre cliente: %s \n",micadena);
	
	comunicacion.Send(cad2,200);
	//filnalizar por dexconesion del cliente
	char fin[TAM_BUFF];
	comunicacion.Receive(fin, TAM_BUFF);
	if(strcmp("Finalizar comunicacion",fin)==0)
		kill(getpid(),SIGUSR1);
}


void CMundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
	//Practica4-MundoServidor no interacciona con el bot(memoriacompartida)
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
/*	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	//Practica2-Jugador1 dispara
	case 'd': {Esfera* d=new Esfera(jugador1.x1+0.1,((jugador1.y1+jugador1.y2)/2),3,0);
		 disparos.push_back(*d);break;}
	case 'l':jugador2.velocidad.y=-4;
		 //Practica4-MundoServidor no interacciona con el bot(memoriacompartida)
		 //pMemC->contador=10.0f;
		 break;
	case 'o':jugador2.velocidad.y=4;
		 //Practica4-MundoServidor no interacciona con el bot(memoriacompartida)
		 //pMemC->contador=10.0f;
		 break;
	//Practica2-Jugador2 dispara
	case 'k':{Esfera *d2=new Esfera(jugador2.x1-0.1,((jugador2.y1+jugador2.y2)/2),-3,0);
		 disparos.push_back(*d2);break;}*/
	}
}

void CMundoServidor::Init()
{
	Plano p;
	//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

	//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	//Practica3-Abrir la tubería en modo escritura
	fd=open("/tmp/mififo",O_WRONLY);
	if(fd==-1)
		perror("Error de apertura mififo\n");
	else {
		printf("Abierta la tubería mififo\n"); }
	//Practica3-Creamos un fichero del tamaño del atributo DatosMemCompartida
	/*Practica4-MundoServidor no interacciona con el bot(memoriacompartida)*/
	//Proyectar el fichero en memoria, uilizando el atributo de la clase
	//Cerrar el descriptor de fichero
	//Asignar la dirección del comienzo de la región creada al atributo de tipo puntero org
	//Guardamos la posicion inicial de la raqueta1 en la memoria compartida
	//Guardamos la posicion inicial de la raqueta y el valor del contador
	//Practica4-abrimos la tubería (servidor-cliente) en modo escritura
	//Practica5-eliminar las tuberías entre cliente y servidor
	/*
	fd2=open("/tmp/mififo2",O_WRONLY);
	if(fd2==-1)
		perror("Error de apertura mififo2\n");
	else {
		printf("Abierta la tubería mififo2\n"); }
	//abrir la tubería (cliente-servidor) en modo lectura
	fd3=open("/tmp/mififo3",O_RDONLY);
	if(fd3==-1)
		perror("Error de apertura mififo3\n");
	else {
		printf("Abierta la tubería mififo3\n"); }
	//crear el thread indicandole la funcion a ejecutar(servidor-cliente)
	pthread_create(&thid1, NULL, hilo_comandos, this);
	*/	

	//Practica4-Ejercicio propuesto señales
	//Aramado de la señal
	struct sigaction act;
	//establece el manejador
	act.sa_handler=matar_proceso;/*funcion a ejecutar*/
	act.sa_flags=0;/*ninguna acción específica*/

	sigemptyset(&act.sa_mask);/*Se inicia el conjunto de señales a bloquear cuando se reciba la señal*/
	sigaction(SIGTERM, &act, NULL);/*señal de terminación*/
	sigaction(SIGINT, &act, NULL);/*señal de atención interactiva*/
	sigaction(SIGPIPE, &act, NULL);/*escritura en la tubería sin lectores*/
	sigaction(SIGUSR1, &act, NULL);/*señal definida por la aplicación*/
	//Practica5-hilos de los primeros jugadores
	pthread_create(&thid1, NULL, hilo_comandos, this);
	//Practica5-enlaza el socket de conexión a una dirección IP y un puerto
	conexion.InitServer((char *)"127.0.0.1",3000);//Comprobar dirección
	
}

//Practica4-implementamos el hilo_comandos()
void* hilo_comandos(void* d)
{
      CMundoServidor* p=(CMundoServidor*) d;
      p->RecibeComandosJugador();
}

/*Como se puede ver, el método RecibeComandosJugador() hace un bucle infinito donde, en cada iteración, lee de la tubería la tecla pulsada y modifica la velocidad del jugador correspondiente.*/
void CMundoServidor::RecibeComandosJugador()
{
     while (1) {
            usleep(10);
            char cad3[TAM_BUFF];
            //read(fd3, cad3, sizeof(cad3));
	    //Practica5-recibe los comandos del cliente es el que recibe los datos de las teclas a través del socket
	    comunicacion.Receive(cad3,TAM_BUFF);
            unsigned char key;
            sscanf(cad3,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
      }
}

//Practica4-Ejercicio propuesto señales (función a ejecutar)
void matar_proceso (int signum) {
	if (signum!=10) {
		printf("El proceso finalizado por la recepción de una señal\nLa señal que ha finalizado el proceso es: %d\n",signum);
		exit(1);
	}
	else {
		printf("El proceso finalizado correctamente\n");
		exit(0);
	}
}
