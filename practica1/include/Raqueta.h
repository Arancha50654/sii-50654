// Raqueta.h: interface for the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#include "Plano.h"
#include "Vector2D.h"

class Raqueta : public Plano  
{
public:
	Vector2D velocidad;

	Raqueta();
	virtual ~Raqueta();

	void Mueve(float t);
	//Practica2-Disminuir el tamaño durante un tiempo
	double tiempo;//tiempo de juego
	double seg;//tiempo que hay que disminuir la raqueta
	void ContarTiempo();
	void Disminuir(float, float, float);
};
