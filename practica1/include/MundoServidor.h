// MundoServidor.h: interface for the CMundoServidor class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "glut.h"
//Practica3-Librerías necesarias para las tuberías con nombre
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
//Practica3-Librerías necesarias para archivos proyectados en memoria
/*Practica4-Quito del servidor las librerías necesarias para archivos proyectados en memoria*/
//Practica4-Librería necesaria para los thread
#include <pthread.h>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
/*Practica4-Al quitar la definición DatosMemCompartida tengo que volver a incluir la definicion Raqueta.h*/
//#include "DatosMemCompartida.h"//Practica3-Incluye la definicion de Raqueta.h
//Practica5-Incuimos la clase Socket
#include "Socket.h"
#include "Raqueta.h"

#define TAM_BUFF 60 //Practica3-Constante con la que definimos el tamaño maximo de la cadena(buffer)

//esta es la clase mundo plagiada por Arancha

class CMundoServidor  
{
public:
	void Init();
	CMundoServidor();
	virtual ~CMundoServidor();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;
	
	//Practica2-Añadir vector disparos
	std::vector<Esfera> disparos;
	//Practica3-Añadimos el identificador de la tubería como atributo de la clase
	int fd;
	//cadena de texto con la que pasaremos los datos por la tubería al logger
	char buff[TAM_BUFF];
	/*Practica4-Quito todos los atributos de la clase que se usasen para realizar la memoria compartida*/
	//Practica3-Añadir el atributo del tipo DatosMemCompartida en Mundo
	//añadir el atributo del tipo puntero a DatosMemCompartida en Mundo
	//añadimos el atributo que utilizaremos para proyectar el fichero en memoria
	/*Practica4-Añadimos el identificador de la tubería como atributo de la clase
	fd2 envia las coordenadas del servidor al cliente (servidor-cliente)
	fd3 envia las teclas pulsadas del cliente al servidor (cliente-servidor)*/
	//Practica5-eliminar las tuberías entre cliente y servidor
	//int fd2,fd3;
	// declarar dos sockets diferentes, uno para la conexión y otro para la comunicación con el cliente.
	Socket conexion, comunicacion;
	//Practica4-añadimor un identificador de thread como atributo de la clase
	pthread_t thid1;
	//añadimos el método
	void RecibeComandosJugador();
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
