//Practica3-Se crea la siguiente estructura de datos en memoria compartidos entre el bot y el juego tenis.
#pragma once 
#include "Esfera.h"
#include "Raqueta.h"
class DatosMemCompartida
{       
public:         
      Esfera esfera;
      Raqueta raqueta1;
      int accion; //1 arriba, 0 nada, -1 abajo
	//Variables necesarias para controlar la raqueta 2 pasado un tiempo sin moverse
	//CONTADOR
	int accion2;
	float contador;
	Raqueta raqueta2;
};
