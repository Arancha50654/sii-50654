// Esfera.h: interface for the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ESFERA_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_)
#define AFX_ESFERA_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Vector2D.h"

class Esfera  
{
public:	
	Esfera();
	//Practica2-Añadir disparos, sobrecarga del constructor
	Esfera(float, float, float, float);
	unsigned r,g,b;//Practica2-color de la esfera (disparos)
	virtual ~Esfera();
		
	Vector2D centro;
	Vector2D velocidad;
	float radio;

	void Mueve(float t);
	void Dibuja();

	//Practica 2-Disminuir tamaño esfera
	void DisminuirTamanio();
};

#endif // !defined(AFX_ESFERA_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_)
